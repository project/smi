<?php

/**
 * @file 
 * Contains Drupal\sm_ui\Entity\SmApp.
 */
namespace Drupal\sm_ui\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityType;
use Drupal\smi_ui\SmAppInterface;

/**
 * Defines a Flower configuration entity class.
 *
 * @ConfigEntityType(
 *   id = "smapp",
 *   label = @Translation("SmApp"),
 *   fieldable = FALSE,
 *   controllers = {
 *     "list_builder" = "Drupal\smi_ui\SmAppListBuilder",
 *     "form" = {
 *       "add" = "Drupal\smi_ui\Form\SmAppForm",
 *       "edit" = "Drupal\smi_ui\Form\SmAppForm",
 *       "delete" = "Drupal\smi_ui\Form\SmAppDeleteForm"
 *     }
 *   },
 *   config_prefix = "smapp",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "app_name"
 *   },
 *   links = {
 *     "edit-form" = "smi_ui.edit_app",
 *     "delete-form" = "smi_ui.delete_app"
 *   }
 * )
 */

class SmApp extends ConfigEntityBase implements SmAppInterface {
  
  /**
   * The ID of the socialmedia application.
   *
   * @var string
   */
  protected $id;
  
  /**
   * The plugin instance settings.
   *
   * @var array
   */
  protected $settings = array ();
  
  /**
   * The plugin instance ID.
   *
   * @var string
   */
  protected $plugin;
  
  /**
   * The application name.
   * @var string
   */
  public $app_name;
  
  /**
   * The application secret.
   * @var string
   */
  public $app_secret;
  

  
   /**
    * {@inheritDoc}
    * @see \Drupal\smi_ui\SmAppInterface::getPlugin()
    */
   public function getPlugin() {
    // TODO: Auto-generated method stub
  }

   /**
    * {@inheritDoc}
    * @see \Drupal\smi_ui\SmAppInterface::getPluginId()
    */
   public function getPluginId() {
    // TODO: Auto-generated method stub
  }

}