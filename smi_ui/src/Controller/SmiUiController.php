<?php
/**
 * @file
 * Contains controller SmiUiController.
 */

namespace Drupal\smi_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
 kint_require();
/**
 * Returns responses for Socialmedia Importer UI module routes.
 *
 * @author afi.amouzou
 */
class SmiUiController extends ControllerBase {

  /**
   *
   * @return multitype:string
   */
  public function applicationsOverview() {

    $query = 'SELECT id, app_name, app_id, app_secret, provider, is_authorized FROM {socialmedia_applications}';
    $results = db_query($query)->fetchAllAssoc('app_name');
    $header = array($this->t('Application Name'), $this->t('Application Id'), $this->t('Application Secret'), $this->t('Provider'), $this->t('Operations'));
    $rows = array();
    foreach ($results as $result) {
      $row = array();
      $deauthorize_url = "admin/config/services/smi/application/" . $result->id . "/deauthorize";
      $authorize_url = "admin/config/services/smi/application/" . $result->id . "/authorize";
      // @FIXME
  // l() expects a Url object, created from a route name or external URI.
  // \Drupal::l(t('edit'), Url::fromRoute('admin.config.services.smi/application/',  array($result->id, "/edit"))),
     // l(t('delete'), "admin/config/services/socialmedia_importer/application/" . $result->id . "/delete"),

     $row[] =  $result->app_name;
     $row[] =  $result->app_id;
     $row[] =  $result->app_secret;
     $row[] =  $result->provider;
     /* $links['edit'] = [
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute('entity.aggregator_feed.edit_form', ['aggregator_feed' => $feed->id()]),
      ];
      $links['delete'] = array(
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('entity.aggregator_feed.delete_form', ['aggregator_feed' => $feed->id()]),
      ); */

     $links['edit'] = [
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute('smi_ui.applications_overview'),
      ];
      $links['delete'] = array(
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('smi_ui.applications_overview'),
      );

      $row[] = array(
        'data' => array(
          '#type' => 'operations',
          '#links' => $links,
        ),
      );
//       $row[] = $result->is_authorized ? l(t('de-authorize'), $deauthorize_url) : l(t('authenticate'), $authorize_url);
      $row[] = $result->is_authorized ? $this->t('de-authorize'): $this->t('authenticate');
      $rows[] = $row;

    }
// \Kint::dump($rows);
    $build['apps'] = array(
      '#prefix' => '<h3>' . $this->t('Aplication overview') . '</h3>',
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No Application available.'),
    );

    return $build;
  }


  /**
   *
   */
  public function addAppPage() {

      // NODE
//       $build = [
//           '#theme' => 'node_add_list',
//           '#cache' => [
//               'tags' => $this->entityManager()->getDefinition('node_type')->getListCacheTags(),
//           ],
//       ];
      
//       $content = array();
      
//       // Only use node types the user has access to.
//       foreach ($this->entityManager()->getStorage('node_type')->loadMultiple() as $type) {
//           $access = $this->entityManager()->getAccessControlHandler('node')->createAccess($type->id(), NULL, [], TRUE);
//           if ($access->isAllowed()) {
//               $content[$type->id()] = $type;
//           }
//           $this->renderer->addCacheableDependency($build, $access);
//       }
      
//       // Bypass the node/add listing if only one content type is available.
//       if (count($content) == 1) {
//           $type = array_shift($content);
//           return $this->redirect('node.add', array('node_type' => $type->id()));
//       }
      
//       $build['#content'] = $content;
      
      $build = array(
        '#theme' => 'smi_ui_app_add_list',
      );
      $content = array();
      $manager = \Drupal::service('plugin.manager.sm_platform');
      $plugins = $manager->getDefinitions();
//       drupal_set_message(print_r($plugins, TRUE));
      
      foreach ($plugins as $provider) {
          $instance = $manager->createInstance($provider['id']);
          drupal_set_message(print_r($instance, TRUE));
          \Kint::dump($instance);
          $content[$provider['id']] = $instance;
//           $build[] = array(
//               '#type' => 'markup',
//               '#markup' => t('<p>Provider @name, description $@description.</p>', array('@name' => $instance->getName(), '@description' => $instance->getDescription())),
//           );
      }
      $build['#content'] = $content;
  	return $build;
  }
  
  /* Provides the application submission form.
  *
  * @param \Drupal\node\NodeTypeInterface $node_type
  *   The node type entity for the node.
  *
  * @return array
  *   An application submission form.
  */
  public function addApp($platform) {
  	$form = array();
//   	$node = $this->entityManager()->getStorage('node')->create(array(
//   			'type' => $node_type->id(),
//   	));
  
//   	$form = $this->entityFormBuilder()->getForm($node);
  
  	return $form;
  }
  
}

