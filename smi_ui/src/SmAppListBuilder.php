<?php
/**
 * @file 
 * Contains Drupal\smi_ui\SmAppListBuilder.
 */

namespace Drupal\smi_ui;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Socialmedia application list builder.
 */
class SmAppListbuilder extends ConfigEntityListBuilder {
  
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = array();
    
    return $header + parent::buildHeader();
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = array();
    $row['label'] = $entity->label();
    $row['app_id'] = $entity->id();
    $row['app_secret'] = $entity->app_secret;
    $row['platform'] = $entity->platform;
    
    return $row + parent::buildRow($entity);
  }
  
  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['#empty'] = $this->t('No Application available');
    
    return $build;
  }
  
}
