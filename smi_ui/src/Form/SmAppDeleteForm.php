<?php
/**
 * @file
 * Contains Drupal\smi_ui\Form\SmAppDeleteForm.
 */

namespace Drupal\smi_ui\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Render\Element\Url;

class SmAppDeleteForm extends EntityConfirmFormBase {
  
 
 /**
  * {@inheritDoc}
  * @see \Drupal\Core\Form\ConfirmFormInterface::getQuestion()
  */
   public function getQuestion() {
    return $this->t('Are you sure you want to delete this application: @name?',
        array('@name' => $this->entity->label()));
  }
  
  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getCancelUrl()
   */
  public function getCancelUrl() {
    return new Url('smi_ui.applications_overview');
    
  }
  
  
  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getConfirmText()
   */
  function getConfirmText() {
    return $this->t('Delete');
    
  }
  

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::submitForm()
   */
  public function submitForm($form, $form_state) {
    $this->entity->delete();
    drupal_set_message($this->t('The application @label has been deleted.', 
        array('@label' => $this->entity->label())));
    $form_state['redirect_route'] = $this->getCancelUrl();
  
  }

}