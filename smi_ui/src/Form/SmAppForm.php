<?php

/**
 * @file 
 * Contains Drupal\smi_ui\SmAppForm
 */
namespace Drupal\smi_ui\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormState;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Provides form for socialmedia aplication.
 */
class SmAppform extends EntityForm {
  
  /**
   * The block entity.
   *
   * @var \Drupal\smi_ui\SmAppInterface
   */
  protected $entity;
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Entity\EntityForm::form()
   */
  public function form($form, $form_state) {
    $form = parent::form ( $form, $form_state );
    $sm_app = $this->entity;
    
    // Change page title for the edit operation
    if ($this->operation == 'edit') {
      $form ['#title'] = $this->t ( 'Edit Application: @name', array (
        '@name' => $sm_app->app_name 
      ) );
    }
    
    // The unique machine name of the application.
    $form['id'] = array(
        '#type' => 'machine_name',
        '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
        '#default_value' => $sm_app->id (),
        '#disabled' => !$sm_app->isNew(),
        '#machine_name' => array(
            'source' => array('name'),
            'exists' => 'sm_app_load'
        ),
    );
    
    
//     $form ['id'] = array (
//       '#type' => 'value',
//       '#value' => $sm_app->id () 
//     );
    
    $form ['provider'] = array (
      '#type' => 'value',
      '#value' => $sm_app->platform 
    );
    $form ['app_name'] = array (
      '#type' => 'textfield',
      '#title' => $this->t( 'Application Name' ),
      '#description' => $this->t ( 'Enter the Name of your Application' ),
      '#default_value' => isset ( $sm_app->app_name ) ? $sm_app->app_name : '',
      '#required' => TRUE 
    );
    $form ['app_id'] = array (
      '#type' => 'textfield',
      '#title' => $this->t ( 'Application Id' ),
      '#default_value' => isset ( $sm_app->app_id ) ? $sm_app->app_id : '',
      '#description' => $this->t ( 'An Application is required to access
      Social Media API. If you dont´t have an Application, you can
      create one in the corresponding Social Media Application page' ),
      '#required' => TRUE 
    );
    $form ['app_secret'] = array (
      '#type' => 'textfield',
      '#title' => $this->t ( 'Applicaion Secret' ),
      '#default_value' => isset ( $sm_app->app_secret ) ? $sm_app->app_secret : '',
      '#description' => $this->t ( 'An Application is required to access
      Social Media API. If you dont´t have an Application, you can
      create one in the corresponding Social Media Application page' ),
      '#required' => TRUE 
    );
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm($form, $form_state) {
    parent::validateForm($form, $form_state);
    
    // The SmApp Entity form puts all block plugin form elements in the
    // settings form element, so just pass that to the block for validation.
    $settings = (new FormState())->setValues($form_state->getValue('settings'));
    // Call the plugin validate handler.
    $this->entity->getPlugin()->validateConfigurationForm($form, $settings);
    // Update the original form values.
    $form_state->setValue('settings', $settings->getValues());
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Entity\EntityForm::submitForm()
   */
  public function submitForm($form, $form_state) {
    
  }
}