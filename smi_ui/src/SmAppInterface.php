<?php

/**
 * @file
 * Contains Drupal\smi_ui\SmAppInterface.
 */
namespace Drupal\smi_ui;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface SmAppInterface extends ConfigEntityInterface {
  
  /**
   * Returns the plugin instance.
   *
   * @return \Drupal\Core\Block\BlockPluginInterface
   *   The plugin instance for this block.
   */
  public function getPlugin();
  
  /**
   * Returns the plugin ID.
   *
   * @return string
   *   The plugin ID for this block.
   */
  public function getPluginId();
  
}