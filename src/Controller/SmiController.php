<?php
/**
 * @file
 * Contains controller SmiController.
 */

namespace Drupal\smi\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Socialmedia Importer Controller.
 *
 * @author afi.amouzou
 */
class SmiController extends ControllerBase {

  /**
   * Get the application data.
   *
   * @param int $app_id
   *   The application id.
   *
   * @return multitype:string
   *   html markup.
   */
  public function getApplicationData($app_id) {
    return array(
      '#markup' => '<p>' . $this->t('Simple page: getApplicationData.') . $app_id .'</p>',
    );
  }

}
