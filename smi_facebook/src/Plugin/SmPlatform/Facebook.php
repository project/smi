<?php
/**
 * @file
 * Contains .
 */

namespace Drupal\smi_facebook\Plugin\SmPlatform;

use Drupal\smi\SmPlatformBase;

/**
 * Provides a 'vanilla' flavor.
 *
 * @SmPlatform(
 *   id = "facebook",
 *   name = @Translation("Facebook"),
 *   description = @Translation("Facebook description")
 * )
 */
class Facebook extends SmPlatformBase {}