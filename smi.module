<?php
/**
 * @file
 * Social Media Importer Module Provides Social Media´s API integration.
 *
 * Import Data from Social Media Twitter, Facebook and Googleplus in JSON.
 * See README.txt and the help page at admin/help/smi.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\DrupalKernel;
use Drupal\Core\Url;

global $base_url;
define('SMI_IMPORT_DATA_URL', $base_url . '/admin/content/smi/import_data');
const SMI_APP_LIST_PATH = 'admin/config/services/smi/app_list';
const SMI_RESPONSE_HANDLER_PATH = 'smi/response_handler/';


/**
 * Implements hook_help().
 */
function smi_help($route_name, RouteMatchInterface $route_match) {
  global $base_url;
  $current_url = Url::fromRoute('<current>');
  // Notes: if ($current_url->toString() != '/admin/help/token').
  $module_path = $base_url . '/' . drupal_get_path('module', 'smi');

  $add_app_link = '';
//   $add_app_url = Url::fromRoute('admin.config.services.smi.application.add');
//   $add_app_link = \Drupal::l(t('Book admin'), $add_app_url);
  $permissions_url = Url::fromRoute('user.admin_permissions', array('fragment' => 'module-smi'));
  $permissions_link = \Drupal::l(t('Permissions'), $permissions_url);

  switch ($route_name) {
    case 'admin.help#smi':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Social Media Importer is a developer only module that
                 allows you to authenticate with social medias e.g. Googleplus,
                 Twitter or Facebook and use this authentication to Import Data
                 in JSON.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Authenticate a Social Media Application') . '</dt>';
      $output .= '<dd>' . t('To get authentication for your Social Media
                 Application fill the <a href="@add-app">Add Application</a>
                 form to get access token. On save of the form it will ask for
                 access, click allow access so that the Application gets authenticated.',
                 array('@add-app' => $add_app_link))
                 . '</dd>';

      $output .= '<dt>' . t('Import Data to Drupal') . '</dt>';
      $output .= '<dd>' . t('Create a module and call the function
                  smi_get_application_data($id). $id is the unique
                  Application id in the database. This is not the app id from the
                  Social Media Application page. The function will return the
                  Application data in JSON.') . '</dd>';

      $output .= '<dt>' . t('User permissions') . '</dt>';
      $output .= '<dd>' . t('The social Media Importer module provides permissions which can be
                  set by role on the <a href="@permissions">permissions page</a>.',
                  array('@permissions' => $permissions_link)) . '</dd>';
      $output .= '</dl>';

      $output .= '<p>' . t("For more details read the
          <a href='@url'>README.txt</a> file in the Social Media Importer
          module directory.", array('@url' => "$module_path/README.txt")) . '</p>';
      return $output;

//     case 'admin.config.services.smi.application.add':
//       $output = '';
//       $output .= '<p>' . t('Use the Application Id and Application Secret
//                   from your Social Media Application setting page to fill
//                   out the Application form. If you do not have a Social
//                   Media Application, you can create one in the corresponding
//                   social media page.') . '</p>';
//       return $output;
  }
}

/**
 * Get all the informations about the defined providers.
 *
 * @see hook_smi_applicaion_info()
 *
 * @return object
 *   An object with this properties
 *    - classes: Associative array of the class of the providers, keyed by the
 *    provider name.
 *    - names: Associative Array of the providers names, keyed by the provider
 *    name.
 *    - name_description: Associative array of the name and description of the
 *     providers, keyed by the provider name.
 */
function _smi_get_application_providers_infos() {
  $application_providers = (object) array(
    'classes' => array(),
    'names' => array(),
    'name_description' => array(),
  );
  $application_provider_infos = array();
  $application_provider_infos = \Drupal::moduleHandler()->invokeAll('smi_application_info', $args = array());

  foreach ($application_provider_infos as $application_provider => $application_data_info) {
    if (array_key_exists('name', $application_data_info) && strlen($application_data_info['name']) > 0) {
      $application_providers->names[$application_provider] = $application_data_info['name'];
      $application_providers->name_description[$application_provider]['name'] = $application_data_info['name'];
      if (array_key_exists('description', $application_data_info)) {
        $application_providers->name_description[$application_provider]['description'] = $application_data_info['description'];
      }

      if (array_key_exists('class', $application_data_info) && strlen($application_data_info['class']) > 0) {
        $application_providers->classes[$application_provider] = $application_data_info['class'];
      }
    }
  }
  return $application_providers;
}

/**
 * Get the name and description of all defined providers.
 *
 * @see hook_smi_applicaion_info()
 *
 * @return array
 *   An associative array of provider name and description keyed
 *   by the provider name.
 */
function smi_get_application_providers_name_desc() {
  $application_providers_infos = _smi_get_application_providers_infos();
  $application_names_desc = array();
  if (property_exists($application_providers_infos, 'name_description')) {
    $application_names_desc = $application_providers_infos->name_description;
  }
  return $application_names_desc;
}

/**
 * Get the handler class name of a Social Media Application Provider.
 *
 * @param string $provider_name
 *   The Provider name.
 *
 * @return string
 *   The Provider handler class name.
 */
function smi_get_application_provider_class($provider_name) {
  $application_providers = _smi_get_application_providers_infos();
  $application_providers_class = $application_providers->classes;
  return $application_providers_class[$provider_name];
}

/**
 * Get a property of all the defined providers.
 *
 * @param string $property
 *   The name of the property: classes, name_description or names.
 *
 * @return array
 *   An array of provider property.
 */
function smi_get_application_providers_properties($property) {
  $application_provider_properties = array();
  if (strlen($property) > 0) {
    $application_providers = _smi_get_application_providers_infos();
    if (property_exists($application_providers, $property)) {
      foreach ($application_providers->$property as $application_provider_property) {
        $application_provider_properties[] = $application_provider_property;
      }
    }
  }
  return $application_provider_properties;
}

/**
 * Save (create new or update) a Social Media Application.
 *
 * @param array $application
 *   An associative array of Application details keyed by
 *   the Application fields names. If id is provided, the Application will
 *   be updated.
 *
 * @return array
 *   Return an array of the saved fields keyed by the Application fields
 *   names or FALSE if the record insert or update failed.
 */
function smi_application_save($application) {
  if (array_key_exists('id', $application) && !is_null($application['id'])) {
    // Update an existing application.
    $is_authorized = 0;
    $id = $application['id'];
    if (array_key_exists('app_secret', $application) || array_key_exists('app_id', $application)) {
      $old_application = socialmedia_importer_application_load($id);
      $old_application_is_authorized = $old_application->is_authorized;
      if ($old_application_is_authorized) {
        $is_authorized = 1;
        // Reset is_authorized field if app_id or app_secret changed.
        if ((isset($application['app_id']) && ($application['app_id'] !== $old_application->app_id))
            || (isset($application['app_secret']) && ($application['app_secret'] !== $old_application->app_secret))) {
          $is_authorized = 0;
        }
      }
      $application['is_authorized'] = $is_authorized;
    }
    \Drupal::moduleHandler()->invokeAll('socialmedia_importer_application_update', [$application]);
    if (\Drupal::database()->merge('socialmedia_applications')->fields($application)->key(['id'])->execute() == SAVED_UPDATED) {
      $application['is_updated'] = TRUE;
      return $application;
    }
    else {
      return FALSE;
    }
  }
  else {
    // Create a new Application.
    \Drupal::moduleHandler()->invokeAll('socialmedia_importer_application_insert', [$application]);
    if (\Drupal::database()->insert('socialmedia_applications')->fields($application)->execute() == SAVED_NEW) {
      $application['is_new'] = TRUE;
      return $application;
    }
    else {
      return FALSE;
    }
  }
}

/**
 * Load Application fields by id.
 *
 * @param int $id
 *   The Application Id.
 *
 * @return object
 *   An Object of Application fields as properties.
 */
function smi_application_load($id) {
  $social_media_application = new SocialMediaImporterApplication($id);
  return $social_media_application->loadApplication();
}

/**
 * Delete an Application.
 *
 * @param int $id
 *   The id of the Application to delete.
 */
function smi_application_delete($id) {
  try {
    \Drupal::moduleHandler()->invokeAll('smi_application_delete', array($id));
    $query = db_delete('socialmedia_applications')
      ->condition('id', $id);
    $query->execute();
    return TRUE;
  }
  catch (Exception $e) {
    $transaction->rollback();
    watchdog_exception('social media importer', $e);
    throw $e;
  }
}

/**
 * Get an instance of a Social Media Application provider.
 *
 * @param int $id
 *   The Application id.
 *
 * @return Ambigous
 *   An Application object or NULL.
 */
function smi_get_application_provider_instance($id) {
  $application_provider_instance = NULL;
  $application_class = NULL;
  $provider = NULL;
  $social_media_application = new SocialMediaImporterApplication($id);
  $provider = $social_media_application->provider;
  if (!is_null($provider)) {
    $application_class = smi_get_application_provider_class($provider);
  }
  if (!is_null($application_class) && class_exists($application_class)) {
    $application_provider_instance = new $application_class($id);
  }
  return $application_provider_instance;
}

/**
 * Deauthorize a Social Media Application.
 *
 * @param int $id
 *   The Application id.
 *
 * @return array
 *   An associative array with keys:
 *     - success: TRUE or FALSE
 *     - (optional) error_message: The error message in case of error.
 */
function smi_deauthorize_application($id) {
  $is_deauthorized = array('success' => FALSE);
  if ($id) {
    $social_media_application = smi_get_application_provider_instance($id);
    if (!is_null($social_media_application)) {
      if ($social_media_application->isAuthorized) {
        // Deauthorize only applications that are already authorized.
        if (method_exists($social_media_application, 'deauthorizeApplication')) {
          $is_deauthorized = $social_media_application->deauthorizeApplication();
          if ($is_deauthorized['success']) {
            $application_data = array(
              'id' => $id,
              'access_token' => NULL,
              'access_token_secret' => NULL,
              'refresh_token' => NULL,
              'is_authorized' => 0,
            );
            $fields = smi_application_save($application_data);
          }
        }
      }
    }
  }
  return $is_deauthorized;
}

/**
 * Get Data for a Social Media Application.
 *
 * @param int $id
 *   The Social Media Application id.
 */
function smi_get_application_data($id) {
  $social_media_application = smi_get_application_provider_instance($id);
  if (!is_null($social_media_application)) {
    $social_media_application->getApplicationData();
  }
}

/**
 * Get the url for social media data import.
 *
 * @param int $id
 *   The Social Media Application id.
 */
function smi_get_data_import_url($id) {
  $url = '';
  if (!is_null($id) || !empty($id)) {
    if ($id > 0) {
      $url = $GLOBALS['base_url'] . '/smi/import_data/' . $id;
    }
  }
  return $url;
}
